package com.example.xademo

import com.example.xademo.repository.AccountRepository
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.core.task.TaskExecutor
import org.springframework.jms.core.JmsTemplate
import org.springframework.test.annotation.Commit
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.transaction.TestTransaction
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
class SenderApplicationTests {

	@MockBean
	lateinit var jmsTemplate : JmsTemplate

	@MockBean
	lateinit var taskExecutor: TaskExecutor

	@MockBean
	lateinit var accountRepository: AccountRepository

	@Autowired
	lateinit var mockMvc: MockMvc

	@Test
	fun testTransactionEventListener() {
		mockMvc.get("/accounts?username=${UUID.randomUUID().toString()}")
				.andExpect { status { isCreated } }
		TestTransaction.flagForCommit()
		TestTransaction.end()
		verify(taskExecutor, times(1)).execute(any())
	}

}
