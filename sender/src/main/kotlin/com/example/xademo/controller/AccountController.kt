package com.example.xademo.controller

import com.example.xademo.controller.mapper.AccountMapper
import com.example.xademo.service.AccountService
import org.mapstruct.factory.Mappers
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.websocket.server.PathParam


@RestController
@RequestMapping("/accounts")
class AccountController(@Autowired val accountService: AccountService) {

    private val accountMapper = Mappers.getMapper(AccountMapper::class.java)

    @GetMapping("")
    fun create(@PathParam("username") username: String): ResponseEntity<Void> {
        accountService.createAccountAndNotify(username)
        return ResponseEntity.status(HttpStatus.CREATED).build()
    }

    @GetMapping("/count")
    fun count(): ResponseEntity<Int> {
        return ResponseEntity.ok(accountService.count())
    }

}
