package com.example.xademo.controller.dto

data class AccountDto(val id: Long?, val username: String) {
    constructor() : this(null, "")
}