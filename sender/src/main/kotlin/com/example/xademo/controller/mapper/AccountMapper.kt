package com.example.xademo.controller.mapper

import com.example.xademo.controller.dto.AccountDto
import com.example.xademo.repository.Account
import org.mapstruct.Mapper

@Mapper
interface AccountMapper {

    fun convertToDto(account: Account): AccountDto

    fun convertToModel(accountDto: AccountDto): Account

}