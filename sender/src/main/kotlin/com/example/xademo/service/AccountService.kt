package com.example.xademo.service

import com.example.xademo.repository.Account
import com.example.xademo.repository.AccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.core.task.TaskExecutor
import org.springframework.jms.core.JmsTemplate
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean
import org.springframework.stereotype.Service
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import javax.annotation.PostConstruct
import javax.sql.DataSource
import javax.transaction.Transactional

@Service
@Transactional
class AccountService(@Autowired val applicationEventPublisher: ApplicationEventPublisher,
                     @Autowired val taskExecutor: TaskExecutor,
                     @Autowired val accountRepository: AccountRepository,
                     @Autowired val dataSource: DataSource,
                     @Autowired val jmsTemplate: JmsTemplate) {

    @Value("\${queue.name}")
    lateinit var queueDestination: String

    @PostConstruct
    fun init() {
        println("Datasource = $dataSource")
    }

    fun createAccountAndNotify(username: String) {
        val account = Account(username)
        accountRepository.save(account)
        jmsTemplate.convertAndSend(queueDestination, username)
        applicationEventPublisher.publishEvent(AccountEvent(account))
    }

    fun count() : Int {
        return accountRepository.count().toInt();
    }

    @TransactionalEventListener(classes = [AccountEvent::class], phase = TransactionPhase.AFTER_COMMIT)
    fun handleApplicationEvent(accountEvent: AccountEvent) {
        taskExecutor.execute {
            println(accountEvent)
        }
    }

    class AccountEvent(val account: Account) {

    }
}