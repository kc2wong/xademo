package com.example.xademo.config

import com.ibm.mq.jms.MQXAConnectionFactory
import com.ibm.msg.client.wmq.WMQConstants
import com.ibm.msg.client.wmq.common.CommonConstants
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.jta.atomikos.AtomikosConnectionFactoryBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jms.core.JmsTemplate
import javax.jms.ConnectionFactory
import javax.jms.JMSException
import javax.jms.XAConnectionFactory

@ConditionalOnProperty(name = ["jms.enabled"], havingValue = "true", matchIfMissing = true)
@Configuration
class JmsConfiguration() {
    @Throws(JMSException::class)
    fun xaConnectionFactory(): XAConnectionFactory {
        val xaConnectionFactory = MQXAConnectionFactory()
        xaConnectionFactory.setIntProperty(CommonConstants.WMQ_CONNECTION_MODE, CommonConstants.WMQ_CM_CLIENT)
        xaConnectionFactory.setStringProperty(WMQConstants.USERID, "admin")
        xaConnectionFactory.setStringProperty(WMQConstants.PASSWORD, "passw0rd")
        xaConnectionFactory.queueManager = "QM1"
        xaConnectionFactory.channel = "DEV.ADMIN.SVRCONN"
        xaConnectionFactory.hostName = "localhost"
        xaConnectionFactory.port = 1414
        return xaConnectionFactory
    }

    @Bean
    @Throws(JMSException::class)
    fun provideConnectionFactory(): ConnectionFactory {
        val bean = AtomikosConnectionFactoryBean()
        bean.xaConnectionFactory = xaConnectionFactory()
        return bean
    }

    @Bean
    @Throws(JMSException::class)
    fun provideJmsTemplate(): JmsTemplate {
        val jmsTemplate = JmsTemplate()
        jmsTemplate.connectionFactory = provideConnectionFactory()
        return jmsTemplate
    }
}