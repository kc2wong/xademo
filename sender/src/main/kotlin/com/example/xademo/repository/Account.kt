package com.example.xademo.repository

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Account(val username: String) {
    @Id
    @GeneratedValue
    private var id: Long? = null

    constructor() : this("")
}