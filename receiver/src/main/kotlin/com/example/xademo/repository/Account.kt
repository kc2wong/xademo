package com.example.xademo.repository

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "ACCOUNT_RECEIVED")
data class Account(val username: String) {
    @Id
    @GeneratedValue
    private var id: Long? = null

    constructor() : this ("")
}