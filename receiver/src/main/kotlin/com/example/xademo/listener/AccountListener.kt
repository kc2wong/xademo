package com.example.xademo.listener

import com.example.xademo.service.AccountService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Component

@Component
class AccountListener(@Autowired val accountService: AccountService) {

    @JmsListener(destination = "\${queue.name}")
    fun receiveAccountMessage(message: String) {
        println("received $message")
        accountService.createAccount(message);
    }
}