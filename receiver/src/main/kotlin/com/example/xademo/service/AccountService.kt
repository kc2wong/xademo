package com.example.xademo.service

import com.example.xademo.repository.Account
import com.example.xademo.repository.AccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.jms.core.JmsTemplate
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class AccountService(@Autowired val accountRepository: AccountRepository) {

    fun createAccount(username: String) {
        accountRepository.save(Account(username))
    }

}